import sys, os
###############################################################################
#kddcup99 dataset records count
#corrected : 311,029 (46MB)
#kddcup.data_10_percent_corrected : 494,021(73MB)
#kddcup.data.corrected : 4,878,432 (725MB)
#                               
###############################################################################

kddcup_column_names = ["duration","protocol_type","service","flag","src_bytes",
                                   "dst_bytes","land","wrong_fragment","urgent","hot","num_failed_logins",
                                   "logged_in","num_compromised","root_shell","su_attempted","num_root",
                                   "num_file_creations","num_shells","num_access_files","num_outbound_cmds",
                                   "is_host_login","is_guest_login","count","srv_count","serror_rate",
                                   "srv_serror_rate","rerror_rate","srv_rerror_rate","same_srv_rate",
                                   "diff_srv_rate","srv_diff_host_rate","dst_host_count","dst_host_srv_count",
                                   "dst_host_same_srv_rate","dst_host_diff_srv_rate","dst_host_same_src_port_rate",
                                   "dst_host_srv_diff_host_rate","dst_host_serror_rate","dst_host_srv_serror_rate",
                                   "dst_host_rerror_rate","dst_host_srv_rerror_rate","label"]
################################################################################################
kddcup99_protocol_types = ['tcp', 'udp', 'icmp']
kddcup99_services          = ['http', 'smtp', 'finger', 'domain_u', 'domain', 'auth', 'telnet',  'ftp', 'eco_i', 'ntp_u', 'ecr_i',
                                     'other','icmp', 'private', 'pop_3', 'ftp_data', 'rje', 'time',  'mtp', 'link', 'remote_job',  'gopher',
                                     'ssh', 'name', 'whois', 'login', 'imap4', 'daytime', 'ctf', 'nntp', 'shell', 'IRC' ,'nnsp', 'http_443',
                                     'exec', 'printer', 'efs', 'courier', 'uucp', 'klogin', 'kshell', 'echo' ,'discard', 'systat','supdup',
                                     'iso_tsap', 'hostnames', 'csnet_ns', 'pop_2' , 'sunrpc', 'uucp_path', 'netbios_ns',
                                     'netbios_ssn', 'netbios_dgm', 'sql_net', 'vmnet', 'bgp', 'Z39_50', 'ldap',
                                     'netstat', 'urh_i', 'X11' ,'urp_i' , 'pm_dump', 'tftp_u' , 'tim_i' ,'red_i', 'http_2784', 'harvest', 'aol', 'http_8001']
kddcup99_flags             = ['SF' ,'S1' ,'REJ', 'S2', 'S0', 'S3' ,'RSTO' ,'RSTR' , 'RSTOS0', 'OTH', 'SH']
################################################################################################
abnormal_probe_attack  = [ 'ipsweep', 'nmap', 'postsweep', 'satan', 'mscan', 'saint']
abnormal_dos_attack     = ['back', 'land', 'neptune', 'pod', 'smurf', 'teardrop', 'mailbomb', 'apache2', 'processtable', 'udpstorm']
abnormal_u2r_attack     = [ 'buffer.overflow', 'localmodule', 'perl', 'rootkit', 'httptunnel', 'xterm', 'ps', 'worm']
abnormal_r2l_attack      = [ 'ftp_write', 'guess_passwd', 'imap', 'multihop' , 'phf' , 'spy', 'warezclient', 'warezmaster', 'snmpgetattack', 'snmpguess', 'xsnoop', 'named', 'sendmail', 'sqlattack', 'xlock']
################################################################################################

batch_size = 100 #500 
validation_batch_size = 10
classes = ['normal', 'probe' ,'dos','u2r','r2l']
classes_size = len(classes)
train_data_column_size = len(kddcup_column_names) -1 #41
################################################################################################
#Convolutional Layer 1.
filter_size1 = 3 
num_filters1 = 4

#Convolutional Layer 2.
filter_size2 = 3
num_filters2 = 4

#Convolutional Layer 3.
filter_size3 = 3
num_filters3 =  8
    
#Fully-connected layer.
fc_size = 64     

#Number of color channels for the images: 1 channel for gray-scale.
num_channels = 3

#image dimensions 
img_size = 4

#Size of image when flattened to a single dimension
img_size_flat = img_size * img_size * num_channels

#training iterations
iterations = 100000 
################################################################################################


