import tensorflow as tf
import numpy as np
import datetime as dt

import kddcup99_dataset as dataset
import kddcup99_config as config

from sklearn.utils import shuffle

orin_data=[]
label_data=[]

###################################################################
now = dt.datetime.now()
#dataset.read_source_data('./test_dummy.txt' ,orin_data ,label_data)
#dataset.read_source_data('./kddcup.data_10_percent_corrected' ,orin_data ,label_data)
dataset.read_source_data('./kddcup.data.corrected' ,orin_data ,label_data)
orin_data = np.array(orin_data)
label_data = np.array(label_data)
train_data = dataset.get_classified_data_remove_label(orin_data)
#normal_train, probe_train, dos_train, u2r_train, r2l_train = dataset.get_classified_data_remove_label(orin_data)
#################################
#print('label_data   : ', label_data.shape)
#print('nomal_train : ', normal_train.shape)
#print('probe_train : ', probe_train.shape)
#print('dos_train    : ', dos_train.shape)
#print('u2r_train    : ', u2r_train.shape)
#print('r2l_train     : ', r2l_train.shape)
#print(label_data)
print("Training-Size:{}".format(len(train_data)))
print("Training-Set:{}".format(len(config.classes)))
print("Labeling-Size:{}".format(len(label_data)))
#################################
elapsed_time = dt.datetime.now() - now
print(elapsed_time)
#################################
label_data, train_data = shuffle(label_data, train_data)
#print('-------------------------------------------------')
#print(label_data)
#label_data, normal_train, probe_train, dos_train, u2r_train, r2l_train = shuffle(label_data, normal_train, probe_train, dos_train, u2r_train, r2l_train)
###################################################################

def new_weights(shape):    
    return tf.Variable(tf.truncated_normal(shape, stddev=0.05))

def new_biases(length):
    return tf.Variable(tf.constant(0.05, shape=[length]))

def new_conv_layer(input, num_input_channels, filter_size,num_filters): 
    shape = [filter_size, filter_size, num_input_channels, num_filters]
    weights = new_weights(shape=shape)     
    biases = new_biases(length=num_filters)

    layer = tf.nn.conv2d(input=input, filter=weights,strides=[1, 1, 1, 1],padding='SAME')
    layer += biases
    
    layer = tf.nn.relu(layer)
    return layer, weights

def flatten_layer(layer):
    layer_shape = layer.get_shape()
    num_features = layer_shape[1:4].num_elements()

    layer_flat = tf.reshape(layer, [-1, num_features])
    return layer_flat, num_features

def new_fc_layer(input, num_inputs, num_outputs, use_relu=True): 
    
    weights = new_weights(shape=[num_inputs, num_outputs])
    biases = new_biases(length=num_outputs)
    layer = tf.matmul(input, weights) + biases

    if use_relu:
        layer = tf.nn.relu(layer)

    return layer

x = tf.placeholder(tf.float32, shape=[None, config.img_size_flat], name='x')
x_data = tf.reshape(x, [-1, config.img_size, config.img_size, config.num_channels])

y_true = tf.placeholder(tf.float32, shape=[None, config.classes_size], name='y_true')
y_true_cls = tf.argmax(y_true, dimension=1)

layer_conv1, weights_conv1 = \
new_conv_layer(input=x_data,
               num_input_channels=config.num_channels,
               filter_size=config.filter_size1,
               num_filters=config.num_filters1,
               )
print("now layer2 input")
print(layer_conv1.get_shape())    

layer_conv2, weights_conv2 = \
new_conv_layer(input=layer_conv1,
               num_input_channels=config.num_filters1,
               filter_size=config.filter_size2,
               num_filters=config.num_filters2,
               )
print("now layer3 input")
print(layer_conv2.get_shape())    

layer_conv3, weights_conv3 = \
new_conv_layer(input=layer_conv2,
               num_input_channels=config.num_filters2,
               filter_size=config.filter_size3,
               num_filters=config.num_filters3,
               )
print("now layer flatten input")
print(layer_conv3.get_shape())     

layer_flat, num_features = flatten_layer(layer_conv3)

layer_fc1 = new_fc_layer(input=layer_flat,
                     num_inputs=num_features,
                     num_outputs=config.fc_size,
                     use_relu=True)

layer_fc2 = new_fc_layer(input=layer_fc1,
                     num_inputs=config.fc_size,
                     num_outputs=config.classes_size,
                     use_relu=False)


y_pred = tf.nn.softmax(layer_fc2,name='y_pred')
y_pred_cls = tf.argmax(y_pred, dimension=1, name='y_pred_cls')

cross_entropy = tf.nn.softmax_cross_entropy_with_logits(logits=layer_fc2 ,labels=y_true, name='cross_entropy')
cost = tf.reduce_mean(cross_entropy, name='cost')

correct_prediction = tf.equal(y_pred_cls, y_true_cls, name='correct_prediction')
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32) , name='accuracy')

optimizer = tf.train.AdamOptimizer(learning_rate=1e-4).minimize(cost)
tf.add_to_collection('optimizer' , optimizer)

batch_size = config.batch_size

session = tf.Session()
session.run(tf.global_variables_initializer()) 

################################################################################################
def print_progress(epoch, feed_dict_train, feed_dict_validate, val_loss):
    acc = session.run(accuracy, feed_dict=feed_dict_train)
    val_acc = session.run(accuracy, feed_dict=feed_dict_validate)
    msg = "Epoch {0} --- Training Accuracy: {1:>6.1%}, Validation Accuracy: {2:>6.1%}, Validation Loss: {3:.3f}"
    print(msg.format(epoch + 1, acc, val_acc, val_loss))
################################################################################################
#total_iterations = 0
validation_batch_size = int(batch_size / 5)
train_batch_size = int(batch_size - validation_batch_size) 

def optimize(num_iterations):

    kddcup_dataset = dataset.dataset_config(train_data, label_data)    
    for i in range(num_iterations):
        print('Iteration : ' + str(i) +'/' + str(num_iterations))
        train_size = train_batch_size
        validation_size = validation_batch_size
        epoch_size , batch_images, batch_labels = kddcup_dataset.next_batch(batch_size)
        if epoch_size < batch_size:
            validation_size = int(epoch_size/5)
            train_size = int(epoch_size - validation_size)
        
        train_images = batch_images[validation_size: ]
        train_labels =  batch_labels[validation_size: ]

        validate_images = batch_images[ : validation_size ]
        validate_labels  = batch_labels[ : validation_size ]

        train_x =  train_images.reshape( train_size, config.img_size_flat)
        validate_x = validate_images.reshape( validation_size, config.img_size_flat)

        feed_dict_train = {x: train_x,    y_true: train_labels}        
        feed_dict_validate = {x: validate_x,   y_true: validate_labels}

        session.run(optimizer, feed_dict=feed_dict_train)
        loss = session.run(cost, feed_dict=feed_dict_validate)
        epoch = int(i / int(kddcup_dataset.size_total_data/epoch_size))            

        print_progress(epoch, feed_dict_train, feed_dict_validate, loss)

        if (i %  (config.batch_size*10)) == 0:
            filename ='.\\kddcup99_test_model'
            filename += ('--' + str(i))
            saver = tf.train.Saver()
            saver.save(session,  filename) 
                


optimize(num_iterations=config.iterations)  


