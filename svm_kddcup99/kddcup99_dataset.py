###############################################################################
#http://kdd.ics.uci.edu/databases/kddcup99/kddcup99.html
#http://kdd.ics.uci.edu/databases/kddcup99/kddcup.names
#https://ntddk.github.io/2016/11/23/kdd-cup-99-data/
##kddcup99 attributes
#http://www.cse.fau.edu/~xqzhu/Stream/kddcup99.arff
###############################################################################

import sys, os, re
import numpy as np
import kddcup99_config as config

################################################################################################
def parsing_data(data, datum):
    
    datum[1] = config.kddcup99_protocol_types.index(datum[1])
    datum[2] = config.kddcup99_services.index(datum[2])
    datum[3] = config.kddcup99_flags.index(datum[3])

    label = datum[41]
    label =re.sub( '[.\n]','', label) 
    if label in config.abnormal_probe_attack:  datum[41]=1
    elif label in config.abnormal_dos_attack:   datum[41]=2
    elif label in config.abnormal_u2r_attack:   datum[41]=3
    elif label in config.abnormal_r2l_attack:    datum[41]=4
    else: datum[41]= 0

    for value in datum:
        data.append(float(value))

    #for data in datum[0 : len(datum)- 1]:
    #    numpy_data.append(float(data))

    return  datum[41]

def read_source_data(file_name,data_list,label_list):
    with open(file_name) as file:
        line_index = 0
        for line in file:
            parsing_list = []
            datum = line.split(',')
            if len(datum) is not len(config.kddcup_column_names):
                continue
            label = parsing_data(parsing_list, datum)
            target = [0]* config.classes_size
            target[label] =1
            data_list.append(parsing_list)
            label_list.append(target)
            #label_list.append(label)
            line_index += 1

def get_classified_data(orin_data):
    train_data = []
    normal_data = []
    probe_data = []
    dos_data = []
    u2r_data = []
    r2l_data = []

    for row in orin_data:
        label = row[41]
        normal_data.append( 1 if label ==0.0 else -1  )
        probe_data.append(  1 if label ==1.0 else -1  )
        dos_data.append(     1 if label ==2.0 else -1  )
        u2r_data.append(     1 if label ==3.0 else -1  )
        r2l_data.append(      1 if label ==4.0 else -1  )

        train_data.append(row[0:-1]) 

    #normal_data = np.array( [1 if y==0 else -1 for y in iris.target] )
    #normal_data = []
    #probe_data = []
    #dos_data = []
    #u2r_data = []
    #r2l_data = []

    #for row in orin_data:
    #    label = row[41]
    #    if label == 0.0:      normal_data.append(row[0:-1])
    #    elif label == 1.0:    probe_data.append(row[0:-1])
    #    elif label == 2.0:    dos_data.append(row[0:-1])
    #    elif label == 3.0:    u2r_data.append(row[0:-1])
    #    elif label == 4.0:    r2l_data.append(row[0:-1])

    return np.array(train_data), np.array(normal_data),np.array(probe_data),np.array(dos_data), np.array(u2r_data),np.array(r2l_data)

def get_classified_data_remove_label(orin_data):
    train_data = []
###############################################################################
# 4 by 4 by 3  = 48     -   41  = 7
# 5 by 5 by 2  = 50    -    41  = 9
# 7 by 7 by 1  = 49    -    41  = 8
###############################################################################

    for row in orin_data:        
        datumn = [0]*(4*4*3)        
        for index in range(41):
            datumn[index] = row[index]

        datumn = np.reshape(datumn, (4, 4, 3))
        train_data.append(datumn)

    return np.array(train_data)
    
class dataset_config:
    def __init__(self, train_data, train_label):
        self.size_total_data = train_data.shape[0]
        self.images = train_data
        self.labels = train_label
        self.index_in_epoch = 0

    def next_batch(self, batch_size):
        start  = self.index_in_epoch * batch_size
        if start >= self.size_total_data :
            self.index_in_epoch = 0
            start = 0

        end = start + batch_size        
        if end >= self.size_total_data :            
            end = self.size_total_data -1
            self.index_in_epoch = 0
        else:
            self.index_in_epoch += 1

        return (end-start) , self.images[start:end], self.labels[start:end]
        


         



