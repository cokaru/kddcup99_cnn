import tensorflow as tf
import numpy as np
import datetime as dt

import kddcup99_dataset as dataset
import kddcup99_config as config
import cnn_evaluate
from sklearn.utils import shuffle

###################################################################
orin_data=[]
label_data=[]
now = dt.datetime.now()
dataset.read_source_data('./corrected' ,orin_data ,label_data)
orin_data = np.array(orin_data)
label_data = np.array(label_data)
validatation_data  = dataset.get_classified_data_remove_label(orin_data)
elapsed_time = dt.datetime.now() - now
print('Loading File-Elasped Time' , elapsed_time)
print("Validation-Size:{}".format(len(validatation_data)))
print("Labeling-Size:{}".format(len(label_data)))

label_data, validatation_data = shuffle(label_data, validatation_data)
###################################################################
restore_file = '.\\kddcup99_test_model--99000.meta'
evaluate_model = cnn_evaluate.model()

session = tf.InteractiveSession()
session.run(tf.global_variables_initializer())

now = dt.datetime.now()
saver, tf_graph = evaluate_model.restore_network(session, restore_file)        
elapsed_time = dt.datetime.now() - now
print('Loading tf.GraphData-Elasped Time' , elapsed_time)
###################################################################
batch_size = config.validation_batch_size
kddcup_dataset = dataset.dataset_config(validatation_data, label_data)    
num_iterations = int(kddcup_dataset.size_total_data / batch_size)
print ('Total Validation Data Size: ' , str(num_iterations))
###################################################################

x = evaluate_model.x
y_true = evaluate_model.y_true
cost = evaluate_model.cost
accuracy = evaluate_model.accuracy
optimizer = evaluate_model.optimizer

for index in range(num_iterations):
    print('Iteration : ' + str(index) +'/' + str(num_iterations))
    epoch_size , batch_images, batch_labels = kddcup_dataset.next_batch(batch_size)
    train_x =  batch_images.reshape( batch_size, config.img_size_flat)
    feed_dict_train = {x: train_x,    y_true: batch_labels}     
    
    loss = session.run(cost, feed_dict=feed_dict_train)
    acc = session.run(accuracy, feed_dict=feed_dict_train)

    msg = 'Training Accuracy: {0:>6.1%}, Loss: {1:.3f}'
    print(msg.format(acc, loss))