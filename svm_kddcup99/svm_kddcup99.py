###############################################################################
#https://simplyml.com/machine-learning-for-network-anomaly-detection/

###############################################################################

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import datetime as dt

import kddcup99_dataset as dataset
import kddcup99_config as config


orin_data  =[]
orin_label =[]

now = dt.datetime.now()
#dataset.read_source_data('./corrected',orin_data,orin_label)
dataset.read_source_data('./test_dummy.txt' ,orin_data ,orin_label)
#dataset.read_source_data('./kddcup.data.corrected',orin_data,orin_label)
###################################################################
#kddcup.data.corrected
##orin_data shape :(4898431, 42)
##orin_label shape :(4898431,)
##normal_train shape :(983233, 41)
##probe_train shape :(30689, 41)
##elapsed time : 0:10:52.610929
###################################################################
orin_data = np.array(orin_data)
orin_label = np.array(orin_label)
###################################################################
print('orin_data shape :' + str(orin_data.shape))
print('orin_label shape :' + str(orin_label.shape))
###################################################################

###################################################################
x_vals , normal_train_y, probe_train_y, dos_train_y, u2r_train_y, r2l_train_y = dataset.get_classified_data(orin_data)
y_vals = np.array([normal_train_y, probe_train_y, dos_train_y,u2r_train_y, r2l_train_y ])
print('x_vals : ' + str(x_vals.shape))
print('y_vals : ' + str(y_vals.shape))
##############################################
#x_vals : (311029, 41)
#y_vals : (5, 311029)
#print('normal_train shape :' + str(normal_train_y.shape))
#print('probe_train shape :'  + str(probe_train_y.shape))
#print('dos_train shape :'  + str(dos_train_y.shape))
#print('u2r_train shape :'  + str(u2r_train_y.shape))
#print('r2l_train shape :'  + str(r2l_train_y.shape))

elapsed_time = dt.datetime.now() - now
print(elapsed_time)
###################################################################


# Initialize placeholders
x_data = tf.placeholder(shape=[None,  config.train_data_column_size], dtype=tf.float32 , name='x_data')
y_target = tf.placeholder(shape=[config.classes_size, None] ,dtype=tf.float32, name='y_target')
#prediction_grid = tf.placeholder(shape=[None, config.train_data_column_size] ,dtype=tf.float32, name='prediction_grid')


#create variables for svm
b = tf.Variable(tf.random_normal(shape=[config.classes_size, config.batch_size]))

#gaussian (RBF) kernel
gamma = tf.constant(-10.0)
sq_dists = tf.multiply(2., tf.matmul(x_data, tf.transpose(x_data)))  # 2c^2
my_kernel = tf.exp(tf.multiply(gamma, tf.abs(sq_dists)))

#declare function to do reshape/batch multiplication
def reshape_matmul(mat):
    v1 = tf.expand_dims(mat, 1)
    v2 = tf.reshape(v1, [config.classes_size, config.batch_size, 1])
    return(tf.matmul(v2, v1))

#compute SVM Model
first_term = tf.reduce_sum(b)
b_vec_cross = tf.matmul(tf.transpose(b), b)
y_target_cross = reshape_matmul(y_target)
second_term = tf.reduce_sum(tf.multiply(my_kernel, tf.multiply(b_vec_cross, y_target_cross)),[1,2])
loss = tf.reduce_sum(tf.negative(tf.subtract(first_term, second_term)) , name='loss')

#gaussian (RBF) prediction kernel
#rA = tf.reshape(tf.reduce_sum(tf.square(x_data), 1),[-1,1])
#rB = tf.reshape(tf.reduce_sum(tf.square(prediction_grid), 1),[-1,1])
#pred_sq_dist = tf.add(tf.subtract(rA, tf.multiply(2., tf.matmul(x_data, tf.transpose(prediction_grid)))), tf.transpose(rB))
#pred_kernel = tf.exp(tf.multiply(gamma, tf.abs(pred_sq_dist)), name='pred_kernel')

#prediction_output = tf.matmul(tf.multiply(y_target,  b), pred_kernel)
#prediction = tf.arg_max(prediction_output-tf.expand_dims(tf.reduce_mean(prediction_output,1), 1), 0 , name='prediction')
#accuracy = tf.reduce_mean(tf.cast(tf.equal(prediction, tf.argmax(y_target,0)), tf.float32) , name='accuracy')

#declare optimizer
optimizer = tf.train.GradientDescentOptimizer(0.01)
train_step = optimizer.minimize(loss)
#tf.add_to_collection('optimizer' , optimizer)

#######################################
##intialize tensorflow 
session = tf.Session()
init = tf.global_variables_initializer()
session.run(init)

#training loop
loss_vec = []
batch_accuracy = []


for i in range(config.iterations):

    #print('Iteration : ' + str(i) +'/' + str(config.iterations))
    rand_index = np.random.choice(len(x_vals), size=config.batch_size)
    rand_x = x_vals[rand_index]
    rand_y = y_vals[:,rand_index]
    session.run(train_step, feed_dict={x_data: rand_x, y_target: rand_y})
    
    temp_loss = session.run(loss, feed_dict={x_data: rand_x, y_target: rand_y})
    loss_vec.append(temp_loss)
    
    #acc_temp = session.run(accuracy, feed_dict={x_data: rand_x,y_target: rand_y,prediction_grid:rand_x})
    #batch_accuracy.append(acc_temp)
    
    if (i%2) == 0:
        print('Step #' + str(i+1))
        print('Loss = ' + str(temp_loss))
        #temp_kernel = session.run(prediction, feed_dict={x_data: rand_x,y_target: rand_y,prediction_grid:rand_x})
        #print(temp_kernel.shape)
        #np.set_printoptions(formatter={'float': '{: 0.4f}'.format})
        #print(temp_kernel)
    
    #if i%30 ==0:
    #    print('Step #' + str(i+1))
    #    print('Loss = ' + str(temp_loss))
    #    saver = tf.train.Saver()
    #    saver.save(session, './kdcup99_test_model')











 











