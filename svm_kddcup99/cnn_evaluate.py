import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np
import math
import kddcup99_dataset
import datetime as dt
import kddcup99_config as config


class model(object):

    def __init__(self):
        self.x = None
        self.y_true = None
        self.y_pred = None
        self.y_pred_cls = None
        self.cost = None
        self.accuracy = None
        self.optimizer = None

    def restore_network(self, session , model_file):
        tf_saver= tf.train.import_meta_graph(model_file)
        tf_saver.restore(session, tf.train.latest_checkpoint('./'))
        tf_graph = tf.get_default_graph()
        self.connect_network(tf_graph)

        return tf_saver, tf_graph

    def connect_network(self, tf_graph):
        self.x = tf_graph.get_tensor_by_name('x:0')
        self.y_true = tf_graph.get_tensor_by_name('y_true:0')
        self.y_pred = tf_graph.get_tensor_by_name('y_pred:0')
        self.y_pred_cls = tf_graph.get_tensor_by_name('y_pred_cls:0')
        self.cost = tf_graph.get_tensor_by_name('cost:0')
        self.accuracy = tf_graph.get_tensor_by_name('accuracy:0')
        self.optimizer = tf.get_collection('optimizer')[0]

    def evalute_prediction(self, session, img_file, img_size):
        images = dataset.load_one_file_prediction(img_file, img_size)
        test_dataset = dataset.DataSet(images, [0], 0, 0 )
        
        user_data = test_dataset._images[0:] 
        user_data = user_data.reshape(1, config.img_size_flat)
        #feed_me = { self.x : user_data, self.y_true : [[0, 0, 0, 0, 0]] }
        feed_me_pred = { self.x : user_data }

        predict = session.run(self.y_pred, feed_dict = feed_me_pred)        
        who_are_you = tf.argmax(predict, dimension=1)
        answer = session.run(who_are_you)
        print(predict)
        print(answer)

        return predict, answer

